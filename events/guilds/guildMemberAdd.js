const { MessageEmbed } = require("discord.js")

module.exports = async (client, member) =>  {
    let channel = member.guild.channels.resolveID(client.settings.WELCOMECHANNELID);
    let embed = new MessageEmbed({
        timestamp: Date.now(),
        thumbnail: member.user.defaultAvatarURL,
        color: "YELLOW",
        author: client,
        fields: [
            {
                name: "Bienvenu "+ member.username +" sur " + member.guild.name,
                value: "Passe un bon moment sur le server :D"
            }
        ]
    })
    channel.send(embed)
}

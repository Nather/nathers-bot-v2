const { MessageEmbed } = require("discord.js");

module.exports.run = (client, message, args) => {
    const embed = new MessageEmbed()
        .setAuthor("Panel d'aide.", 'https://zupimages.net/up/20/20/e532.png')
        .setColor('#2bfafa')
        .setDescription("Voici les liens utile")
        .setImage('https://zupimages.net/up/20/21/5aee.png')
        .addFields(
            { name: "[**SITEWEB**]", value: "https://www.aero-network.com/"},
            { name: "[**INVITATION**]", value: "https://discord.gg/RUGkCJY"},
            { name: "[**SERVEUR**]", value: "play.aero-network.com"},
            { name: "[**VERSION**]", value: "1.12.2 à la 1.15.x"}
        )
    message.channel.send(embed)
}

module.exports.help = {
    name: 'info',
    description: "Renvoie les information sur le server",
    category: "misc",
    args: false,
    cooldown: 10,
    aliases: ["information", "info"],
    permissions: false
}
